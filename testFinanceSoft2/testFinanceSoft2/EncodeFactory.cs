﻿using System;
namespace testFinanceSoft2.Properties
{
    public abstract class EncodeFactory
    {
        public abstract IFactory getEncoding(string encoding);
    }
}
