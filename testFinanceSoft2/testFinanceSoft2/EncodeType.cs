﻿using System;
namespace testFinanceSoft2
{
    public class EncodeType
    {
        private EncodeStrategy encodeStrategy;

        public void setEncodeStrategy(EncodeStrategy encodeStrategy)
        {
            this.encodeStrategy = encodeStrategy;
        }

        public string encodeData(String str)
        {
            return this.encodeStrategy.encodeString(str);
        }
    }
}
