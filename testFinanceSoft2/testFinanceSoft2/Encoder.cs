﻿using System;
namespace testFinanceSoft2
{
    public class Encoder : EncodeStrategy
    {
        public override string encodeString(string str)
        {
            string res = "";
            foreach (char ch in str)
            {
                char s = ch;
                s++;
                res += s;
            }
            return res;
        }
    }
}
