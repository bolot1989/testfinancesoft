﻿using System;

namespace testFinanceSoft2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            EncodeType encodeType = new EncodeType();
            string strToEncode = "abcd";

            encodeType.setEncodeStrategy(new Encoder());
            Console.WriteLine("Ordinary Encoder: {0}", encodeType.encodeData(strToEncode));

            encodeType.setEncodeStrategy(new SHA1Encoder());
            Console.WriteLine("SHA1 Encoder: {0}", encodeType.encodeData(strToEncode));
        }
    }
 }
