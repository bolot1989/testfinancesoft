﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace testFinanceSoft2
{
    public class SHA1Encoder : EncodeStrategy
    {
        public override string encodeString(string str)
        {
            SHA1 sha1 = SHA1.Create();
            byte[] hashData = sha1.ComputeHash(Encoding.Default.GetBytes(str));

            StringBuilder returnValue = new StringBuilder();

            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }
            return returnValue.ToString();
        }
    }
}
