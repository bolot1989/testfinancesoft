﻿using System;
namespace testFinanceSoft
{
    public class Encoder
    {
        public string EncodeString(string str)
        {
            string res = "";
            foreach (char ch in str)
            {
                char s = ch;
                s++;
                res += s;
            }
            return res;
        }
    }
}
