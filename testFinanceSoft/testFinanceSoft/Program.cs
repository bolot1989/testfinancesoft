﻿using System;

namespace testFinanceSoft
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            String someData = "abcd";

            Console.WriteLine(reverseForString(someData));
        }

        // 1. reverse string using for loop
        private static String reverseForString(string str) {
            string reverseStr = "";

            for (int i = str.Length - 1; i >= 0; i--) {
                reverseStr += str[i];
            }

            return reverseStr;
        }

        // 2. reverse string using recursion
        private static String reverseRecursionString(string str) {
            if(str.Length > 0) {
                return str[str.Length - 1] + reverseRecursionString(str.Substring(0, str.Length - 1));
            }
            else {
                return str;
            }
        }

        // 3. reverse string using built-in functions
        private static String reverseBuiltInString(string str) {
            char[] tempChr = str.ToCharArray();
            Array.Reverse(tempChr);

            return new string(tempChr);
        }

        // 4. reverse string using Stack data structure
        private static String reverseStringStack(string str) {
            int top = 0;
            char[] charArray = new char[100];

            void push(char x) {
                if (top > 99)
                    throw new System.IndexOutOfRangeException();

                charArray[++top] = x;
            }

            char pop() {
                return charArray[top--];
            }

            char[] array = str.ToCharArray();
            for(int i = 0; i < str.Length; i++) {
                push(array[i]);
            }
            String reverseStr = "";

            for (int i = 0; i < str.Length; i++)  
            {  
                reverseStr += pop();  
            }

            return reverseStr;
        }
    }
}
